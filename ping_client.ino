/**
 * A Mirf example to test the latency between two Ardunio.
 *
 * Pins:
 * Hardware SPI:
 * MISO -> 12
 * MOSI -> 11
 * SCK -> 13
 *
 * Configurable:
 * CE -> 8
 * CSN -> 7
 *
 * Note: To see best case latency comment out all Serial.println
 * statements not displaying the result and load 
 * 'ping_server_interupt' on the server.
 */

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>



/**
 * debug leds 
 */

int STTS_LED_PIN = 5; 

/**
 * define struct as data model to send
 */
typedef struct {
  int Value1;
  int Value2;
} 
RedioData;
RedioData dataToSend;

/************************************************************
 * 
 * WELCOME TO THE JUNGLE
 * 
 **************************************************************/

void setup(){


  Serial.begin(115200);

  /**
   * Define status led pin as OP 
   */

  pinMode(STTS_LED_PIN, OUTPUT);

  /*
   * Setup pins / SPI.
   */

  /* To change CE / CSN Pins:
   * 
   * Mirf.csnPin = 9;
   * Mirf.cePin = 7;
   */

  Mirf.cePin = 7;
  Mirf.csnPin = 8;
  /* */
  Mirf.spi = &MirfHardwareSpi;
  Mirf.init();

  /*
   * Configure reciving address.
   */

  Mirf.setRADDR((byte *)"clie1");

  /*
   * Set the payload length to sizeof(unsigned long) the
   * return type of millis().
   *
   * NB: payload on client and server must be the same.
   */

  Mirf.payload = sizeof(dataToSend);

  /*
   * Write channel and payload config then power up reciver.
   */

  /*
   * To change channel:
   * 
   * Mirf.channel = 10;
   *
   * NB: Make sure channel is legal in your area.
   */

  Mirf.config();
  /**
   * Do blink led for 10 times 
   */
  Serial.println("Beginning ... "); 
  //  showStartupSeq ();


}

void loop(){



  unsigned long time = millis();

  Mirf.setTADDR((byte *)"serv1");

  //  Mirf.send((byte *)&time);
  Serial.println("Sending Data");
  Serial.print("Value1:  ");
  Serial.print(dataToSend.Value1);
  Serial.print("&  Value2:  ");
  Serial.print(dataToSend.Value2);
  Serial.println("\n============");



  //  digitalWrite(STTS_LED_PIN, HIGH);
  while(Mirf.isSending()){

  }
  //  digitalWrite(STTS_LED_PIN, LOW);
  Serial.println("Finished sending");
  delay(10);
  while(!Mirf.dataReady()){
    //Serial.println("Waiting");
    if ( ( millis() - time ) > 1000 ) {
      Serial.println("Timeout on response from server!");
      showError();
      return;
    }
  }


  Serial.print("Ping: ");
  Serial.println((millis() - time));
  if(!Mirf.isSending() && Mirf.dataReady()){
    Mirf.getData((byte *)&dataToSend);
  }

  delay(1000);
} 

/***
 * DEBUDGing
 * 
 */
void showStartupSeq (void) {
  digitalWrite(STTS_LED_PIN, LOW);    // make sure its off

  for (int i = 0/*seq counter*/; i < 10 ; i++) {

    digitalWrite(STTS_LED_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);              // wait for a second
    digitalWrite(STTS_LED_PIN, LOW);    // turn the LED off by making the voltage LOW
    delay(100);            // wait for a second
  }  

}  


void showError (void) {
  digitalWrite(STTS_LED_PIN, LOW);    // make sure its off
  for (int i = 0/*seq counter*/; i < 10 ; i++) {

    digitalWrite(STTS_LED_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);              // wait for a second
    digitalWrite(STTS_LED_PIN, LOW);    // turn the LED off by making the voltage LOW
    delay(50);            // wait for a second
  }  
}

void showTransmission (void) {
  digitalWrite(STTS_LED_PIN, LOW);    // make sure its off
  for (int i = 0/*seq counter*/; i < 20 ; i++) {

    digitalWrite(STTS_LED_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(50);              // wait for a second
    digitalWrite(STTS_LED_PIN, LOW);    // turn the LED off by making the voltage LOW
    delay(50);            // wait for a second
  }  
}


/*
Cmd
 */

void listernForCMD(void) {
  /*
    while(Mirf.isSending()){
   
   }
   */
  if(!Mirf.isSending() && Mirf.dataReady()){

    Mirf.getData((byte *)&dataToSend);
    executeCMD();
  }

}

void executeCMD(void) {



  if(dataToSend.Value1 == 1){

    digitalWrite(STTS_LED_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)

  } 
  else {

    digitalWrite(STTS_LED_PIN, LOW);   // turn the LED on (HIGH is the voltage level)
  }

  dataToSend.Value2 = 1;

  Mirf.setTADDR((byte *)"serv1");

  Mirf.send((byte *)&dataToSend);
  while(Mirf.isSending()){

  }

}



